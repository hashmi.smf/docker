FROM ruby:2.4.1

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN mkdir /docker

WORKDIR /docker

COPY Gemfile /docker/Gemfile

COPY Gemfile.lock /docker/Gemfile.lock

RUN bundle install

COPY . /docker

